package com.luv2code.thymleaf.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="employee")
public class Employee {

    // define fields

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @Column(name="first_name")
    private String firstName;

    @Column(name="last_name")
    private String lastName;

    @Column(name="email")
    private String email;


    @Column(name = "gender")
    private String gender;
    @Column(name = "salary")
    private long salary;

    @Column(name = "age")
    int age;

    @Column(name = "active")
    boolean active = true;

    @ManyToOne
    @JoinColumn( name = "designation_id" )
    private Designation designation;



    @OneToMany
    @JoinTable(name = "employee_address",
            joinColumns = {@JoinColumn(name = "employee_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "adress_id", referencedColumnName = "id")}
    )
    private List<Address> addresses = new ArrayList<>();








    // define constructors

    public Employee() {

    }

    public Employee(String firstName, String lastName, String email,  String gender, long salary, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;

        this.gender = gender;
        this.salary = salary;
        this.age = age;
    }

    // define getter/setter


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public long getSalary() {
        return salary;
    }

    public void setSalary(long salary) {
        this.salary = salary;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Designation getDesignation() {
        return designation;
    }

    public void setDesignation(Designation designation) {
        this.designation = designation;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }


    public void addAddress(Address address)
    {
        if (addresses.contains(address))
        {

        }
        else {
            addresses.add(address);
        }
    }



    public void removeAddress(Address address)
    {
        addresses.remove(address);
    }


    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", gender='" + gender + '\'' +
                ", salary=" + salary +
                ", age=" + age +
                '}';
    }


}