package com.luv2code.thymleaf.entity;

import javax.persistence.*;

@Entity
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    int id;
    @Column(name = "city")
    String city;
    @Column(name = "street")
    String street;
    @Column(name = "house_no")
    String House_No;
    @Column(name = "active")
    boolean active = true;

    @ManyToOne
    @JoinTable(name = "employee_address",
            joinColumns = {@JoinColumn(name = "adress_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "employee_id", referencedColumnName = "id")}
    )
    private Employee employee;




    public Address() {
    }

    public Address(String city, String street, String house_No) {
        this.city = city;
        this.street = street;
        House_No = house_No;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouse_No() {
        return House_No;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public void setHouse_No(String house_No) {
        House_No = house_No;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
