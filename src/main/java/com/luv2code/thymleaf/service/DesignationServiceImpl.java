package com.luv2code.thymleaf.service;

import com.luv2code.thymleaf.dto.DesignationDto;
import com.luv2code.thymleaf.entity.Designation;
import com.luv2code.thymleaf.entity.Employee;
import com.luv2code.thymleaf.exception.ResourceNotFoundException;
import com.luv2code.thymleaf.repository.DesignationRepository;
import com.luv2code.thymleaf.repository.EmployeeRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class DesignationServiceImpl implements DesignationService {

    DesignationRepository designationRepository;
    EmployeeRepository employeeRepository;

    @Autowired
    public DesignationServiceImpl(DesignationRepository designationRepository, EmployeeRepository employeeRepository) {
        this.designationRepository = designationRepository;
        this.employeeRepository = employeeRepository;
    }




    @Override
    public List<DesignationDto> findAll() {
        List<Designation> designationList= designationRepository.findByActiveTrue();
        List<DesignationDto> designationDtoList = new ArrayList<>();
        for (Designation designation:designationList)
        {
            DesignationDto designationDto = new DesignationDto();
            BeanUtils.copyProperties(designation,designationDto);
            designationDtoList.add(designationDto);
        }
        return designationDtoList;
    }

    @Override
    public Designation findByid(int theId) {
        Designation designation = null;
        Optional<Designation> result = designationRepository.findById(theId);
        if (result.isPresent())
        {
            designation=result.get();
            return designation;
        }
        else {
            throw new ResourceNotFoundException("the designation is not found by the id = "+theId);
        }
    }

    @Override
    public void save(Designation designation) {
      designationRepository.save(designation);

    }

    @Override
    public void deleteById(int theId) {
       try {
           Designation designation = findByid(theId);
           designation.setActive(false);
           designationRepository.save(designation);
           List<Employee> employees = employeeRepository.findAllByDesignationId(theId);

           if (employees!=null) {
               for (Employee employee : employees) {
                   employee.setDesignation(null);
                   employeeRepository.save(employee);
               }
           }


       }
       catch (Exception e)
       {
           e.printStackTrace();
       }
    }
}
