package com.luv2code.thymleaf.service;

import com.luv2code.thymleaf.dto.DesignationDto;
import com.luv2code.thymleaf.entity.Designation;
import com.luv2code.thymleaf.entity.Employee;
import org.springframework.stereotype.Service;

import java.util.List;


public interface DesignationService {
    public List<DesignationDto> findAll();

    public Designation findByid(int theId);

    public void save(Designation designation);

    public void deleteById(int theId);
}
