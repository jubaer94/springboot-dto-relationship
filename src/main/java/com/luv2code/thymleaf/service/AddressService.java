package com.luv2code.thymleaf.service;

import com.luv2code.thymleaf.dto.AddressDto;
import com.luv2code.thymleaf.dto.DesignationDto;
import com.luv2code.thymleaf.entity.Designation;

import java.util.List;

public interface AddressService {

    public List<AddressDto> findAll();

    public AddressDto findByid(int theId);

    public void save(AddressDto addressDto);

    public void deleteById(int theId);
}
