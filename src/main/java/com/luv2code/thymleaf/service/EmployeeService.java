package com.luv2code.thymleaf.service;

import com.luv2code.thymleaf.dto.EmployeeDto;
import com.luv2code.thymleaf.entity.Employee;
import org.springframework.stereotype.Service;

import java.util.List;


public interface EmployeeService {
    public List<EmployeeDto> findAll();

    public EmployeeDto findByid(int theId);

    public void save(EmployeeDto theEmployeeDto);

    public void deleteById(int theId);
}
