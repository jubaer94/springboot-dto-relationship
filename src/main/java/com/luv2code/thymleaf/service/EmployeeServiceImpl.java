package com.luv2code.thymleaf.service;

import com.luv2code.thymleaf.dto.AddressDto;
import com.luv2code.thymleaf.dto.EmployeeDto;
import com.luv2code.thymleaf.entity.Address;
import com.luv2code.thymleaf.entity.Designation;
import com.luv2code.thymleaf.entity.Employee;
import com.luv2code.thymleaf.exception.ResourceNotFoundException;
import com.luv2code.thymleaf.repository.EmployeeRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    private EmployeeRepository employeeRepository;
    private DesignationService designationService;


    @Autowired
    public EmployeeServiceImpl(EmployeeRepository employeeRepository, DesignationService designationService) {
        this.employeeRepository = employeeRepository;
        this.designationService = designationService;

    }

    @Override

    public List<EmployeeDto> findAll() {

        List<EmployeeDto> employeeDtoList = new ArrayList<>();

       List<Employee> employeeList = employeeRepository.findByActiveTrue();

       for (Employee employee : employeeList)
       {
           EmployeeDto employeeDto = new EmployeeDto();
           BeanUtils.copyProperties(employee,employeeDto);
           employeeDtoList.add(employeeDto);
       }

       return employeeDtoList;


    }

    @Override

    public EmployeeDto findByid(int theId) {
        // TODO Auto-generated method stub
        Employee employee = null;
        EmployeeDto employeeDto = new EmployeeDto();
        Optional<Employee> Result = employeeRepository.findById(theId);

        if (Result.isPresent()) {

            employee = Result.get();
        }
        else {
            //we did'nt find the employee id

            throw new ResourceNotFoundException("employee not found with id = "+theId);
        }
        BeanUtils.copyProperties(employee,employeeDto);
        List<Address> addressList = employee.getAddresses();
        List<AddressDto> addressDtoList = new ArrayList<>();
        for (Address address :addressList)
        {
            AddressDto addressDto = new AddressDto();
            BeanUtils.copyProperties(address , addressDto);
            addressDtoList.add(addressDto);
        }
        
        employeeDto.setAddressDtoList(addressDtoList);


        return employeeDto;
    }


    @Override

    public void save(EmployeeDto theEmployeeDto) {
        // TODO Auto-generated method stub

        if (theEmployeeDto.getId()==0) {
            Employee employee = new Employee();
            BeanUtils.copyProperties(theEmployeeDto, employee);
            Designation designation = designationService.findByid(theEmployeeDto.getDesignation().getId());

            employee.setDesignation(designation);

            employeeRepository.save(employee);
        }
        else
        {

            Employee employee = new Employee();
            Employee employee1 = employeeRepository.getOne(theEmployeeDto.getId());
            List<Address> addressList = employee1.getAddresses();
            BeanUtils.copyProperties(theEmployeeDto, employee);

            Designation designation = designationService.findByid(theEmployeeDto.getDesignation().getId());
            
            employee.setDesignation(designation);
            employee.setAddresses(addressList);


            employeeRepository.save(employee);
        }

    }

    @Override

    public void deleteById(int theId) {

        Employee employee=null;
        Optional<Employee> result = employeeRepository.findById(theId);
        if (result.isPresent())
        {
            employee = result.get();
            List<Address> addressList = employee.getAddresses();
            if (addressList!=null)
            {
                for (Address address : addressList)
                {
                    address.setEmployee(null);
                    address.setActive(false);
                }
            }
            employee.setActive(false);

            employeeRepository.save(employee);

        }

        else
        {
            throw new ResourceNotFoundException("employee not found by id = "+theId);
        }

    }

}
