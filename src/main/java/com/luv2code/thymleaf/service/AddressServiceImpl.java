package com.luv2code.thymleaf.service;

import com.luv2code.thymleaf.dto.AddressDto;
import com.luv2code.thymleaf.dto.EmployeeDto;
import com.luv2code.thymleaf.entity.Address;
import com.luv2code.thymleaf.entity.Employee;
import com.luv2code.thymleaf.repository.AddressRepository;
import com.luv2code.thymleaf.repository.EmployeeRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AddressServiceImpl implements AddressService {

    AddressRepository addressRepository;
    EmployeeRepository employeeRepository;

    @Autowired
    public AddressServiceImpl(AddressRepository addressRepository, EmployeeRepository employeeRepository) {
        this.addressRepository = addressRepository;
        this.employeeRepository = employeeRepository;
    }

    @Override
    public List<AddressDto> findAll() {
        List<AddressDto> addressDtoList = new ArrayList<>();
        List<Address> addressList = addressRepository.findByActiveTrue();
        for (Address address:addressList)
        {
            AddressDto addressDto = new AddressDto();
            Employee employee = address.getEmployee();
            EmployeeDto employeeDto = new EmployeeDto();

             BeanUtils.copyProperties(address,addressDto);
            BeanUtils.copyProperties(employee,employeeDto);
            addressDto.setEmployeeDto(employeeDto);
            addressDtoList.add(addressDto);

        }
        return addressDtoList;
    }

    @Override
    public AddressDto findByid(int theId) {
        AddressDto addressDto = new AddressDto();
        Employee employee = new Employee();
        EmployeeDto employeeDto = new EmployeeDto();
        Address address = new Address();
        Optional<Address> result = addressRepository.findById(theId);
        if (result.isPresent())
        {
            address = result.get();
        }
        employee = address.getEmployee();
        BeanUtils.copyProperties(address , addressDto);
        BeanUtils.copyProperties(employee , employeeDto);
        addressDto.setEmployeeDto(employeeDto);
       return addressDto;
    }

    @Override
    public void save(AddressDto addressDto) {

        Employee employee = new Employee();
        Address address = new Address();
        Optional<Employee> result = employeeRepository.findById(addressDto.getEmployeeDto().getId());
        if (result.isPresent())
        {
            employee = result.get();
            BeanUtils.copyProperties(addressDto,address);
            address.setEmployee(employee);
            addressRepository.save(address);



        }
        else {
            throw  new RuntimeException("Not found");
        }


    }

    @Override
    public void deleteById(int theId) {

        Optional<Address> result = addressRepository.findById(theId);
        Address address = new Address();
        Employee employee = new Employee();
        if (result.isPresent())
        {
            address = result.get();
            Optional<Employee> result2 = employeeRepository.findById(address.getEmployee().getId());

            if (result2.isPresent())
            {
                employee = result2.get();
                address.setActive(false);
                address.setEmployee(null);
                addressRepository.save(address);
            }
            else {
                throw  new RuntimeException("employee not found ");
            }


        }

        else {
            throw new RuntimeException("address not found with id = "+theId);
        }



    }
}
