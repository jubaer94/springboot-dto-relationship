package com.luv2code.thymleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = {"com.luv2code.thymleaf.init","com.luv2code.thymleaf.controller" ,"com.luv2code.thymleaf.service" ,"com.luv2code.thymleaf.exception"})
@EnableJpaRepositories(basePackages = "com.luv2code.thymleaf.repository")
@EntityScan(basePackages = "com.luv2code.thymleaf.entity")
public class ThymleafApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThymleafApplication.class, args);
	}

}
