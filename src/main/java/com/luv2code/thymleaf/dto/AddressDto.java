package com.luv2code.thymleaf.dto;

import com.luv2code.thymleaf.entity.Employee;

import javax.persistence.*;

public class AddressDto {

    int id;
    String city;
    String street;
    String House_No;
    boolean active = true;
    private EmployeeDto employeeDto;

    public AddressDto(int id, String city, String street, String house_No, boolean active, EmployeeDto employeeDto) {
        this.id = id;
        this.city = city;
        this.street = street;
        House_No = house_No;
        this.active = active;
        this.employeeDto = employeeDto;
    }

    public AddressDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouse_No() {
        return House_No;
    }

    public void setHouse_No(String house_No) {
        House_No = house_No;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public EmployeeDto getEmployeeDto() {
        return employeeDto;
    }

    public void setEmployeeDto(EmployeeDto employeeDto) {
        this.employeeDto = employeeDto;
    }
}
