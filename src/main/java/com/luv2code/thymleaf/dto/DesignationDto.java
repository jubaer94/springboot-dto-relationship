package com.luv2code.thymleaf.dto;

import lombok.Data;
import org.springframework.stereotype.Component;

@Component
@Data
public class DesignationDto {

    private int id;
    private String designation;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }
}
