package com.luv2code.thymleaf.dto;

import com.luv2code.thymleaf.entity.Address;
import com.luv2code.thymleaf.entity.Designation;
import lombok.Data;
import org.springframework.stereotype.Component;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.List;

@Data
@Component
public class EmployeeDto implements Serializable {

    public EmployeeDto() {
    }

    private int id;

    private String firstName;

    private String lastName;

    private String email;

    private String gender;

    private long salary;

    int age;

    private Designation designation;

    List<AddressDto> addressDtoList;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public long getSalary() {
        return salary;
    }

    public void setSalary(long salary) {
        this.salary = salary;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Designation getDesignation() {
        return designation;
    }

    public void setDesignation(Designation designation) {
        this.designation = designation;
    }

    public List<AddressDto> getAddressDtoList() {
        return addressDtoList;
    }

    public void setAddressDtoList(List<AddressDto> addressDtoList) {
        this.addressDtoList = addressDtoList;
    }

    public void addAddress(AddressDto address) {
        if (addressDtoList.contains(address)) {
            throw new RuntimeException("the adress is already added before");
        } else {
            addressDtoList.add(address);
        }
    }


}