package com.luv2code.thymleaf.repository;

import com.luv2code.thymleaf.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee , Integer> {

    // add a method to sort by last name


    public List<Employee> findAllByOrderByLastNameAsc();
    List<Employee> findByActiveTrue();
    List<Employee> findAllByDesignationId(int designation_id);
}
