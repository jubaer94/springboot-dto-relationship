package com.luv2code.thymleaf.repository;

import com.luv2code.thymleaf.entity.Designation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DesignationRepository extends JpaRepository<Designation,Integer> {

    List<Designation> findByActiveTrue();
}
