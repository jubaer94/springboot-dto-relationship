package com.luv2code.thymleaf.repository;

import com.luv2code.thymleaf.entity.Address;
import com.luv2code.thymleaf.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AddressRepository extends JpaRepository<Address , Integer> {

    List<Address> findByActiveTrue();

}
