package com.luv2code.thymleaf.controller;

import com.luv2code.thymleaf.dto.DesignationDto;
import com.luv2code.thymleaf.dto.DesignationDto;

import com.luv2code.thymleaf.entity.Designation;

import com.luv2code.thymleaf.entity.Employee;
import com.luv2code.thymleaf.service.DesignationService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/designations")
public class DesignationController {

    
    DesignationService designationService;

    @Autowired
    public DesignationController(DesignationService designationService) {
        this.designationService = designationService;
    }

    @GetMapping("/list")
    public String DesignationList(Model model)
    {
        List<DesignationDto> designations = designationService.findAll();
        model.addAttribute("designations",designations);
        return "/designations/designation-list";

    }

    @GetMapping("/showFormForAdd")
    public String  showFormForAdd(Model theModel)
    {
        // Employee employee = new Employee();
        DesignationDto designationDto = new DesignationDto();
        theModel.addAttribute("designation",designationDto);
        String[] roles = {"Manager" , "Admin" ,"Staff"};
        theModel.addAttribute("roles",roles);

        return "designations/designation-form";
    }
    @PostMapping("/save")
    public String saveDesignation(@ModelAttribute("designation") DesignationDto designationDto)
    {
        Designation designation = new Designation();
        BeanUtils.copyProperties(designationDto, designation);
        designationService.save(designation);
        return "redirect:/designations/list";
    }


    @GetMapping("/showFormForUpdate/{id}")
    public String  showFormForUpdate(@PathVariable("id") int id ,Model model)
    {
        DesignationDto designationDto = new DesignationDto();
        Designation designation = designationService.findByid(id);
        BeanUtils.copyProperties(designation,designationDto);
        model.addAttribute("designation",designationDto);
        String[] roles = {"Manager" , "Admin" ,"Staff"};
        model.addAttribute("roles",roles);
        return "designations/designation-form";

    }
//    @PostMapping("/update/{id}")
//    public String updateDesignation(@PathVariable("id")int id ,@ModelAttribute("designation") Designation designation)
//    {
//        designation.setId(id);
//        designationService.save(designation);
//        return "redirect:/designations/list";
//
//    }
    @GetMapping("/delete/{id}")
    public String deleteDesignation(@PathVariable("id") int id )
    {
        designationService.deleteById(id);
        return "redirect:/designations/list";
    }

}
