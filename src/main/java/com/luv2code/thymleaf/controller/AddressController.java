package com.luv2code.thymleaf.controller;

import com.luv2code.thymleaf.dto.AddressDto;
import com.luv2code.thymleaf.dto.EmployeeDto;
import com.luv2code.thymleaf.entity.Employee;
import com.luv2code.thymleaf.service.AddressService;
import com.luv2code.thymleaf.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
@RequestMapping("/address")
public class AddressController {

    AddressService addressService;
    EmployeeService employeeService;

    @Autowired
    public AddressController(AddressService addressService, EmployeeService employeeService) {
        this.addressService = addressService;
        this.employeeService = employeeService;
    }




    @GetMapping("/list")
    public String listAddress(Model model)
    {

        List<AddressDto> addressDtoList = addressService.findAll();
        model.addAttribute("addressList" , addressDtoList );

        return "/address/address-list";
    }

    @GetMapping("/showFormForAdd/{employeeId}")
    public String  showFormForAdd(@PathVariable("employeeId") int employeeId, Model theModel)
    {
        EmployeeDto employeeDto = employeeService.findByid(employeeId);
        AddressDto addressDto = new AddressDto();
        addressDto.setEmployeeDto(employeeDto);
        theModel.addAttribute("address",addressDto);

        return "/address/address-form";

    }

    @GetMapping("/showFormForUpdate/{id}")
    public String  showFormForUpdate(@PathVariable("id") int addressId, Model theModel)
    {

        AddressDto addressDto = addressService.findByid(addressId);
        EmployeeDto employeeDto = new EmployeeDto();
        if (addressDto.getEmployeeDto().getId()!=0)
        {
             employeeDto = employeeService.findByid(addressDto.getEmployeeDto().getId());
        }
        addressDto.setEmployeeDto(employeeDto);

        theModel.addAttribute("address" , addressDto);

        return "/address/address-form";

    }


    @PostMapping("/save")
    public String saveAddress(@ModelAttribute("address") AddressDto addressDto , RedirectAttributes redirectAttributes)
    {

          addressService.save(addressDto);
          redirectAttributes.addAttribute("id" ,addressDto.getEmployeeDto().getId());
          return "redirect:/employees/view/{id}";
    }
    @GetMapping("/delete/{id}")
    public String deleteAddress(@PathVariable("id") int id , RedirectAttributes redirectAttributes )
    {

        AddressDto addressDto = addressService.findByid(id);
        redirectAttributes.addAttribute("id" ,addressDto.getEmployeeDto().getId());
       addressService.deleteById(id);
        System.out.println("hitting delete controller");
        return "redirect:/employees/view/{id}";
    }

}
