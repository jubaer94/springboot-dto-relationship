package com.luv2code.thymleaf.controller;

import com.luv2code.thymleaf.dto.AddressDto;
import com.luv2code.thymleaf.dto.DesignationDto;
import com.luv2code.thymleaf.dto.EmployeeDto;
import com.luv2code.thymleaf.exception.ResourceNotFoundException;
import com.luv2code.thymleaf.service.AddressService;
import com.luv2code.thymleaf.service.DesignationService;
import com.luv2code.thymleaf.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/employees")
public class EmployeeController {


    EmployeeService employeeService;

    DesignationService designationService;

    AddressService addressService;



    @Autowired
    public EmployeeController(EmployeeService employeeService, DesignationService designationService, AddressService addressService) {
        this.employeeService = employeeService;
        this.designationService = designationService;
        this.addressService = addressService;

    }




    @GetMapping("/list")
    public String  listEmployees(Model theModel)
    {
        List<EmployeeDto> employees = employeeService.findAll();
        theModel.addAttribute("employees",employees);
        return "/employees/list-employees";
    }

    @GetMapping("/showFormForAdd")
    public String  showFormForAdd(Model theModel)
    {
       // Employee employee = new Employee();
        EmployeeDto employeeDto = new EmployeeDto();
        List<DesignationDto> designations = designationService.findAll();

        theModel.addAttribute("employee",employeeDto);

//        String[] roles = {"Manager" , "Admin" ,"Staff"};
        String[] genders = {"Male" , "Female"};
//        theModel.addAttribute("roles",roles);
        theModel.addAttribute("genders" , genders);

        theModel.addAttribute("designations" , designations);


        return "employees/employee-form";

    }

    @PostMapping("/save")
     public String saveEmployee(@ModelAttribute("employee") EmployeeDto employeeDto)
    {
//       // Employee employee = new Employee();
//        BeanUtils.copyProperties(employeeDto, employee);
//        Designation designation = designationService.findByid(employeeDto.getDesignation().getId());
//        employee.setDesignation(designation);

        employeeService.save(employeeDto);

        return "redirect:/employees/list";
    }

    @GetMapping("/showFormForUpdate/{id}")
    public String  showFormForUpdate(@PathVariable("id") int id ,Model model)
    {
      //Employee employee = employeeService.findByid(id);
      EmployeeDto employeeDto = employeeService.findByid(id);

      if (employeeDto==null)
      {
          throw new ResourceNotFoundException("the employee not found with id = "+id);
      }
     // BeanUtils.copyProperties(employee,employeeDto);
        List<DesignationDto> designations = designationService.findAll();
      model.addAttribute("employee",employeeDto);
//        String[] roles = {"Manager" , "Admin" ,"Staff"};
        String[] genders = {"Male" , "Female"};
//        model.addAttribute("roles",roles);
        model.addAttribute("designations" , designations);
        model.addAttribute("genders" , genders);
//        List<CourseDto> courseDtoList = courseService.findAll();
//        model.addAttribute("courses",courseDtoList);
        return "employees/employee-form";

    }
    @GetMapping("/view/{id}")
    public String viewEmployee(@PathVariable("id") int id , Model theModel  )
    {
        EmployeeDto employeeDto = employeeService.findByid(id);

        if (employeeDto==null)
        {
            throw  new RuntimeException("employee not found with id " +id) ;
        }
        List<AddressDto> addressDtoList = employeeDto.getAddressDtoList();
        theModel.addAttribute("employee" , employeeDto);
        theModel.addAttribute("addressList" , addressDtoList);

        return "employees/view-employee";

    }

//   @PostMapping("/update/{id}")
//    public String updateEmployee(@PathVariable("id")int id ,@ModelAttribute("employee") Employee employee)
//   {
//       employee.setId(id);
//       employeeService.save(employee);
//       return "redirect:/employees/list";
//
//   }
   @GetMapping("/delete/{id}")
    public String deleteEmployee(@PathVariable("id") int id )
   {
       employeeService.deleteById(id);
       return "redirect:/employees/list";
   }







}
